import json
import boto3
import uuid
import datetime

def lambda_handler(event, context):

    date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    #dynamodb variables
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('OutboundEmail')

    #generate uuid
    emailId = uuid.uuid4().hex
    
    try:
        #request variables
        to = event['Request']['Email']['to']
        _from = event['Request']['Email']['from']
        subject = event['Request']['Email']['subject']
        body = event['Request']['Email']['body']

        table.put_item(
            Item={
                'emailId':emailId,
                'datetime':date,
                'to':to,
                'from': _from,
                'subject': subject,
                'body': body
            }
        )
    except:
        return {
            "statusCode": 500,
            "body": json.dumps(
                {
                    "message": "Failure"
                }
            ),
        }    

    else:
        return {
            "statusCode": 200,
            "body": json.dumps(
                {
                    "message": "Success"
                }
            ),
        }